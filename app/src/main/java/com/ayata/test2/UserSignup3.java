package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class UserSignup3 extends AppCompatActivity {

    private Button btn;
    private RadioButton r1,r2;
    private TextView text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_signup_3);

        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.us3_toolbar);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.tb_text);
        headerText.setText("Your Information");


        btn = findViewById(R.id.us3_btn_submit);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("next","click");
                Intent i= new Intent(UserSignup3.this,CongratulationsUser.class);
                startActivity(i);
                finish();
            }
        });


        r1=(RadioButton)findViewById(R.id.us3_driving_license);
        r2=(RadioButton)findViewById(R.id.us3_passport);
        text=(TextView)findViewById(R.id.us3_add_photo_text);

        //driving license selected as default
        r1.setChecked(true);
        text.setText("Add Driving License Photo");
        getSupportFragmentManager().beginTransaction().add(R.id.us3_fragment,new UserSignup3FragmentDrivingLicense()).commit();


        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r1.setChecked(true);
               // r2.setChecked(false);
                text.setText("Add Driving License Photo");
                getSupportFragmentManager().beginTransaction().replace(R.id.us3_fragment,new UserSignup3FragmentDrivingLicense()).commit();

            }
        });

        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //r1.setChecked(false);
                r2.setChecked(true);
                text.setText("Add Passport Photo");
                getSupportFragmentManager().beginTransaction().replace(R.id.us3_fragment,new UserSignup3FragmentPassport()).commit();
            }
        });


    }
}
