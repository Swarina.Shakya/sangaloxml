package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MyEvents extends AppCompatActivity {

    //private Button btn;
    private View lineCurrent;
    private View linePast;
    private TextView textCurrent;
    private TextView textPast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_events);



        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.my_events_toolbar);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("My Events");

        ImageView back=(ImageView)headerLayout.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("myevent back","click");
                Intent i= new Intent(MyEvents.this,Profile.class);
                startActivity(i);
                finish();
            }
        });


//        btn = findViewById(R.id.my_events_btn);
//
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("next","click");
//                Intent i= new Intent(MyEvents.this,EventCreateFirst.class);
//                startActivity(i);
//                finish();
//            }
//        });


        FloatingActionButton fab = findViewById(R.id.my_events_btn_floating);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("floating btn","click");
                Intent i= new Intent(MyEvents.this,EventCreateFirst.class);
                startActivity(i);
                finish();
            }
        });



        //select one option

        textCurrent=(TextView)findViewById(R.id.my_events_current_text);
        textCurrent.setTextColor(getResources().getColor(R.color.colorAccent));
        lineCurrent=findViewById(R.id.my_events_current_line);
        lineCurrent.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_my_events,new MyEventsFragmentCurrent()).commit();

        textPast=(TextView)findViewById(R.id.my_events_past_text);
        linePast=findViewById(R.id.my_events_past_line);


        LinearLayout current=(LinearLayout)findViewById(R.id.my_events_current);
        current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //current layout
                textCurrent.setTextColor(getResources().getColor(R.color.colorAccent));
                lineCurrent.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                //past layout
                textPast.setTextColor(getResources().getColor(R.color.text_light));
                linePast.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                //fragment current
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_my_events,new MyEventsFragmentCurrent()).commit();

            }
        });

        LinearLayout past=(LinearLayout)findViewById(R.id.my_events_past);
        past.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //current layout
                textCurrent.setTextColor(getResources().getColor(R.color.text_light));
                lineCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                //past layout

                textPast.setTextColor(getResources().getColor(R.color.colorAccent));
                linePast.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                //fragment past
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_my_events,new MyEventsFragmentPast()).commit();

            }
        });


    }
}
