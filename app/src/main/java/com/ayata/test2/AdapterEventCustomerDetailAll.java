package com.ayata.test2;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterEventCustomerDetailAll extends RecyclerView.Adapter<AdapterEventCustomerDetailAll.MyViewHolder> {
    List<ModelEventCustomerDetailAll> list_ecda;
    public AdapterEventCustomerDetailAll(List list_ecda) {
        this.list_ecda=list_ecda;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_event_customer_detail_all_fragment, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.name.setText(list_ecda.get(position).getName());
        holder.date.setText(list_ecda.get(position).getDate());
        holder.ticketno.setText(list_ecda.get(position).getTicketno());
    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
////        ModelEventCustomerDetailAll data=list_ecda.get(position);
////        holder.name.setText(list_ecda.get(position).getName());
////        holder.date.setText(list_ecda.get(position).getDate());
////        holder.ticketno.setText(list_ecda.get(position).getTicketno());
//    }

    @Override
    public int getItemCount() {
        return list_ecda.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,date,ticketno;
        public MyViewHolder(View itemView) {
            super(itemView);
            name=(TextView) itemView.findViewById(R.id.ecda_current_eventname);
            date=(TextView) itemView.findViewById(R.id.ecda_current_eventdate);
            ticketno=(TextView) itemView.findViewById(R.id.edca_current_ticketno);
        }
    }
}
