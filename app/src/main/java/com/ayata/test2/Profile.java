package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.profile1_appbar);
        TextView headerText= (TextView)headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("My Profile");


//        Toolbar toolbar=(Toolbar) findViewById(R.id.appbar)
//        setSupportActionBar(toolbar);
//
//

        TextView myevent =(TextView) findViewById(R.id.profile_myevents);
        myevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_event = new Intent(Profile.this,MyEvents.class);
                startActivity(intent_event);
                finish();
            }
        });

        TextView business =(TextView) findViewById(R.id.profile_business);
        business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_event = new Intent(Profile.this,CreateBusiness.class);
                startActivity(intent_event);
                finish();
            }
        });

        LinearLayout mybusiness =(LinearLayout) findViewById(R.id.profile_mybusiness);
        mybusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_event = new Intent(Profile.this,NewBusinessSingle.class);
                startActivity(intent_event);
                finish();
            }
        });

        LinearLayout mytickets =(LinearLayout) findViewById(R.id.profile_mytickets);
        mytickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_event = new Intent(Profile.this,EventCustomerDetailAll.class);
                startActivity(intent_event);
                finish();
            }
        });

        LinearLayout help =(LinearLayout) findViewById(R.id.profile_help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this,Help.class);
                startActivity(i);
                finish();
            }
        });

        LinearLayout privacypolicy =(LinearLayout) findViewById(R.id.profile_privacy);
        privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this,PrivacyPolicy.class);
                startActivity(i);
                finish();
            }
        });
        LinearLayout security =(LinearLayout) findViewById(R.id.profile_security);
        security.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this,Security.class);
                startActivity(i);
                finish();
            }
        });

    }
}
