package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class PrivacyPolicy extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);

        View headerLayout= findViewById(R.id.privacy_policy_include);
        TextView headerText= (TextView)headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("Privacy Policy");

        imageView = headerLayout.findViewById(R.id.back);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PrivacyPolicy.this, Profile.class);
                startActivity(i);
                finish();
            }
        });
    }
}
