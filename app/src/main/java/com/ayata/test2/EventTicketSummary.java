package com.ayata.test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EventTicketSummary extends AppCompatActivity {

   private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_ticket_summary);
        btn=findViewById(R.id.btn_eventticketsummary_next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EventTicketSummary.this,EventTicketQR.class);
                startActivity(i);
            }
        });
    }
}
