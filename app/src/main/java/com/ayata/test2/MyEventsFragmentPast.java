package com.ayata.test2;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyEventsFragmentPast.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyEventsFragmentPast#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyEventsFragmentPast extends Fragment implements AdapterMyEvents.OnEventListener {

    private RecyclerView recyclerView;
    private AdapterMyEvents adapterMyEvents;
    private List<ModelMyEvents> listmodel;
    private LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_events_fragment_past, container, false);

        recyclerView = view.findViewById(R.id.recycle_my_events);
        listmodel=new ArrayList<>();
        adapterMyEvents=new AdapterMyEvents(listmodel,this);
        RecyclerView.LayoutManager manager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapterMyEvents);
        DataPrepare();
        return view;
    }

    private void DataPrepare(){
        ModelMyEvents data=new ModelMyEvents(R.drawable.event_image,"New Year Eve","2020/01/02");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
        data=new ModelMyEvents(R.drawable.event_image,"Halloween","2019/11/12");
        listmodel.add(data);
    }

    @Override
    public void onEventClick(int position) {
        Log.d("myEventsPast","clicked");
        Intent intent= new Intent(getContext(),EventCustomerList.class);
        startActivity(intent);
    }
}
