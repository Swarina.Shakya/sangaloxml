package com.ayata.test2;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class EventCreateFirst extends AppCompatActivity {

    private Button btn;

   // map --- if version match or not
    private static final String TAG = "MainActivity";
    private static final int ERROR_DIALOG_REQUEST = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_create_first);

        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.event_create_first_toolbar);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.tb_text);
        headerText.setText("Create Event");


        //nextbtn

//        btn = findViewById(R.id.ec_btn_submit);
//
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("next","click");
//                Intent i= new Intent(EventCreateFirst.this,MapActivity.class);
//                startActivity(i);
//                finish();
//            }
//        });

        //to init map in another page
        //check service first

        if (isServicesOK()){
            initMap();
        }
    }


    //map init
    private void initMap(){
        Button btnmap=(Button) findViewById(R.id.ec_btn_submit);
        btnmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EventCreateFirst.this, MapActivity.class);
                startActivity(intent);
            }
        });
    }

    //        MAP---SERVICE CHECK
    public boolean isServicesOK(){
        Log.d(TAG,"isServiceSOK: CHECKING GOOGLE SERVICES VERSION");

        int available= GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(EventCreateFirst.this);

        if(available== ConnectionResult.SUCCESS){
            //user can request map
            Log.d(TAG,"isServicesOK: GOOGLE PLAY SERVICES IS WORKING");
            return true;
        }

        else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //error occured but we can solve it
            Log.d(TAG,"isServicesOK: error occured but we can solve it");
            Dialog dialog= GoogleApiAvailability.getInstance().getErrorDialog(EventCreateFirst.this,available,ERROR_DIALOG_REQUEST);
            dialog.show();
        }
        else {
            Toast.makeText(this,"You can't make map request",Toast.LENGTH_SHORT).show();
        }
        return false;
    }


}
