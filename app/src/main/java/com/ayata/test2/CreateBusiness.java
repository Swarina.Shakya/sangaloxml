package com.ayata.test2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
public class CreateBusiness extends AppCompatActivity {
    private TextView termsOs;
    private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_business);
//     termsOs =findViewById(R.id.terms_of_service);
//     String text ="By clicking Get Started, you agree to the our Terms of Service.";
//        SpannableStringBuilder ss= new SpannableStringBuilder(text);
//        ForegroundColorSpan fs=new ForegroundColorSpan(Color.BLUE);
//        ss.setSpan(fs,42,57, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//    termsOs.setText(ss);
        btn=findViewById(R.id.btn_createbusiness);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CreateBusiness.this,BusinessCreate.class);
                startActivity(i);
                finish();
            }
        });
    }
}