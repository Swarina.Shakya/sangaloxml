package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class EventCustomerList extends AppCompatActivity implements AdapterEventCustomerList.OnCustomerListener  {
    private Button btn;

    private LinearLayoutManager linearLayoutManager;
    private List<ModelEventCustomerList> listitem;
    private AdapterEventCustomerList adapterEventCustomerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_customer_list);

        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.ecl_appbar);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("My Attendees");


//        LinearLayout menu_photos = (LinearLayout )findViewById(R.id.ecl_client);
//        menu_photos.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent picture_intent = new Intent(EventCustomerList.this,MyEventCustomerDetail.class);
//                startActivity(picture_intent );
//            }
//        });

        RecyclerView recyclerView= (RecyclerView)findViewById(R.id.ecl_recyclerview);
        listitem= new ArrayList<>();
        adapterEventCustomerList= new AdapterEventCustomerList(this,listitem,this);
        linearLayoutManager= new LinearLayoutManager(this);
        recyclerView.setAdapter(adapterEventCustomerList);
        recyclerView.setLayoutManager(linearLayoutManager);
        DataPrepare();

    }

    private void DataPrepare(){
        ModelEventCustomerList data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);
        data=new ModelEventCustomerList(R.drawable.man_1,"John Doe","TK-5854252");
        listitem.add(data);

    }


    @Override
    public void onCustomerClick(int position) {
        Intent intent=new Intent(EventCustomerList.this,MyEventCustomerDetail.class);
        startActivity(intent);
        finish();
    }
}
