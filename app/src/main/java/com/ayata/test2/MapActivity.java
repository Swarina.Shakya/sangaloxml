package com.ayata.test2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback{

    private static final String tag = "MainActivity";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;


    //variables
    private Boolean locationPermissionGranted = false;
    private GoogleMap mMap;

    private Button btn;


    //method override for onmapreadycallback

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this,"Map is ready",Toast.LENGTH_SHORT).show();
        mMap= googleMap;

//        LatLng home=new LatLng(27.708480, 85.305539);
//        mMap.addMarker(new MarkerOptions().position(home).title("home"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(home));
}


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_create_map);
        getLocationPermission();

        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.events_create_map_toolbar);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.tb_text);
        headerText.setText("Select Address");


        //nextbtn

        btn = findViewById(R.id.events_create_map_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("next","click");
                Intent i= new Intent(MapActivity.this,EventsCreateTicketType.class);
                startActivity(i);
                finish();
            }
        });
    }

    public void initMap(){
        Log.d(tag,"initializing map");
        SupportMapFragment mapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync( MapActivity.this);
    }


    //explicitly check permission
    private void getLocationPermission() {

        Log.d(tag,"getting the location");
        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationPermissionGranted = true;
            } else {
                ActivityCompat.requestPermissions(this,
                        permission,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }

        }
        else {
            ActivityCompat.requestPermissions(this,
                    permission,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Log.d(tag,"onRequestPermissionResult: called");
        locationPermissionGranted=false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0) {

                    for (int i=0; i< grantResults.length;i++){
                        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                            locationPermissionGranted=false;
                            Log.d(tag,"onRequestPermissionResult: permission failed");
                            return;
                        }
                    }

                    Log.d(tag,"onRequestPermissionResult: permission granted");

                    locationPermissionGranted= true;
                    //initialise map

                    initMap();
                }
            }
        }
    }

}


