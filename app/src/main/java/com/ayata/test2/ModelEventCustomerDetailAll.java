package com.ayata.test2;


public class ModelEventCustomerDetailAll {
    private String name;
    private String date;
    private String ticketno;

    public ModelEventCustomerDetailAll() {
    }

    public ModelEventCustomerDetailAll(String name, String date, String ticketno) {
        this.name = name;
        this.date = date;
        this.ticketno = ticketno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTicketno() {
        return ticketno;
    }

    public void setTicketno(String ticketno) {
        this.ticketno = ticketno;
    }



}
