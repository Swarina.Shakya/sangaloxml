package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class BusinessLogin extends AppCompatActivity {
private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_businesslogin);
                View headerLayout= findViewById(R.id.business_login_include);
        TextView headerText= (TextView)headerLayout.findViewById(R.id.login);
        headerText.setText("Business Login");

        View login= findViewById(R.id.business_login_include);

        btn=login.findViewById(R.id.button_login);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BusinessLogin.this,NewBusinessSingle.class);
                startActivity(i);
                finish();
            }
        });

        TextView textView= login.findViewById(R.id.signup_text);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BusinessLogin.this,CreateBusiness.class);
                startActivity(i);
                finish();

            }
        });

    }
}
