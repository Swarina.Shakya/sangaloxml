package com.ayata.test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BusinessCreate2 extends AppCompatActivity {
private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_create2);

        View headerLayout = findViewById(R.id.bc2_include);

        TextView headerText=(TextView)headerLayout.findViewById(R.id.tmn_textView11);
        headerText.setText("More Business Details");
        btn=findViewById(R.id.btn_businesscreate2_next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(BusinessCreate2.this,BusinessChoosePlan.class);
                startActivity(i);
                finish();
            }
        });
    }
}
