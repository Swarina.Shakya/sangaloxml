package com.ayata.test2;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventCustomerDetailAllFragmentPast extends Fragment {

    private RecyclerView recyclerView;
    private AdapterEventCustomerDetailAll ecda_Adapter;
    private List<ModelEventCustomerDetailAll> list_ecda = new ArrayList<>();

    public EventCustomerDetailAllFragmentPast() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_customer_detail_all_fragment_past, container, false);
        recyclerView = view.findViewById(R.id.recycler_view_ecda);
        ecda_Adapter = new AdapterEventCustomerDetailAll(list_ecda);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(ecda_Adapter);
        DataPrepare();
        return view;
    }

    private void DataPrepare() {
        ModelEventCustomerDetailAll data = new ModelEventCustomerDetailAll("event1", "2019/8/7", "2");
        list_ecda.add(data);
        data = new ModelEventCustomerDetailAll("event2", "2012/8/6", "8");
        list_ecda.add(data);
        data = new ModelEventCustomerDetailAll("event3", "2012/2/3", "10");
        list_ecda.add(data);

    }
}