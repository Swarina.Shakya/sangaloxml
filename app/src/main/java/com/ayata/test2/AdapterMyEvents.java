package com.ayata.test2;

import android.content.Context;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterMyEvents extends RecyclerView.Adapter<AdapterMyEvents.modelViewHolder> {

    //Context context;
    List<ModelMyEvents> listmodel;

    //clicklistener
    private OnEventListener onEventListener;

//    public AdapterMyEvents(Context context, List<ModelMyEvents> listmodel) {
//        this.context = context;
//        this.listmodel = listmodel;
//    }

    public AdapterMyEvents(List<ModelMyEvents> listmodel, OnEventListener onEventListener) {
        this.listmodel = listmodel;
        this.onEventListener=onEventListener;
    }

    @NonNull
    @Override
    public modelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_my_events,parent,false);
        return new modelViewHolder(v,onEventListener);
    }

    @Override
    public void onBindViewHolder(@NonNull modelViewHolder holder, int position) {
        holder.image.setImageResource(listmodel.get(position).getImage());
        holder.eventname.setText(listmodel.get(position).getEventname());
        holder.eventdate.setText(listmodel.get(position).getEventdate());
    }

    @Override
    public int getItemCount() {
        return listmodel.size();
    }

    public class modelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView eventname;
        TextView eventdate;
        ImageView image;
        OnEventListener onEventListener;

        public modelViewHolder(@NonNull View itemView,OnEventListener onEventListener) {
            super(itemView);
            image= itemView.findViewById(R.id.my_events_event_pic);
            eventname=(TextView)itemView.findViewById(R.id.my_events_event_name);
            eventdate=(TextView)itemView.findViewById(R.id.my_events_event_date);

            //clicklistener
            this.onEventListener=onEventListener;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onEventListener.onEventClick(getAdapterPosition());
        }
    }

    public interface OnEventListener{
        void onEventClick(int position);
    }


}
