package com.ayata.test2;

public class ModelHelp {

    public enum HelpType{
        type_open,type_close;
    }

    String question;
    HelpType helpType;

//    public ModelHelp(String question) {
//        this.question = question;
//    }


    public ModelHelp(String question, HelpType helpType) {
        this.question = question;
        this.helpType = helpType;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public HelpType getHelpType() {
        return helpType;
    }

    public void setHelpType(HelpType helpType) {
        this.helpType = helpType;
    }
}
