package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MyEventCustomerDetail extends AppCompatActivity {
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_event_customer_detail);

        //view ma text change in toolbar

        View headerLayout = findViewById(R.id.mecd_appbar);

        TextView headerText = (TextView) headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("My Attendee");


        btn = findViewById(R.id.mecd_btn_ok);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent picture_intent = new Intent(MyEventCustomerDetail.this, MyEvents.class);
                startActivity(picture_intent);
            }
        });

        ImageView back=(ImageView)headerLayout.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("back","click");
                Intent i= new Intent(MyEventCustomerDetail.this,EventCustomerList.class);
                startActivity(i);
                finish();
            }
        });

    }
}
