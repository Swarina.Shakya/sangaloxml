package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Security extends AppCompatActivity {
    private ImageView imageView;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.security);

        View headerLayout= findViewById(R.id.security_include);
        TextView headerText= (TextView)headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("Security");

        imageView = headerLayout.findViewById(R.id.back);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Security.this, Profile.class);
                startActivity(i);
                finish();
            }
        });

        btn=findViewById(R.id.btn_security_cancel);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Security.this,Profile.class);
                startActivity(i);
                finish();
            }
        });

        btn=findViewById(R.id.btn_security_save);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Security.this,Profile.class);
                startActivity(i);
                finish();
            }
        });
    }
}
