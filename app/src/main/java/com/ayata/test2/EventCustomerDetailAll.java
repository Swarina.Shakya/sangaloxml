package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EventCustomerDetailAll extends AppCompatActivity {

    private Button btnCurrent;
    private Button btnPast;
    private TextView textCurrent;
    private TextView textPast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_customer_detail_all);



        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.ecda_appbar);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("My Tickets");


        btnPast=findViewById(R.id.ecda_past_line);
        textPast=findViewById(R.id.ecda_past_text);
        //select one option

        textCurrent=(TextView)findViewById(R.id.ecda_current_text);
        textCurrent.setTextColor(getResources().getColor(R.color.colorAccent));
        btnCurrent=findViewById(R.id.ecda_current_line);
        btnCurrent.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_ecda,new EventCustomerDetailAllFragmentCurrent()).commit();

        LinearLayout current=(LinearLayout)findViewById(R.id.ecda_current);
        current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //current layout
                textCurrent=(TextView)findViewById(R.id.ecda_current_text);
                textCurrent.setTextColor(getResources().getColor(R.color.colorAccent));
                btnCurrent=findViewById(R.id.ecda_current_line);
                btnCurrent.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                //past layout
                textPast=(TextView)findViewById(R.id.ecda_past_text);
                textPast.setTextColor(getResources().getColor(R.color.text_light));
                btnPast=findViewById(R.id.ecda_past_line);
                btnPast.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                //fragment current
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_ecda,new EventCustomerDetailAllFragmentCurrent()).commit();

            }
        });

        LinearLayout past=(LinearLayout)findViewById(R.id.ecda_past);
        past.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //current layout
                textCurrent.setTextColor(getResources().getColor(R.color.text_light));
                btnCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                //past layout
                textPast.setTextColor(getResources().getColor(R.color.colorAccent));
                btnPast.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                //fragment past
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_ecda,new EventCustomerDetailAllFragmentPast()).commit();

            }
        });

    }
}