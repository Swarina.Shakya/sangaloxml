package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AdvertisementForm extends AppCompatActivity implements View.OnClickListener{
    RadioButton rb_1,rb_2,rb_3,rb_4;

    private Button btn;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.advertisementform);

            init();

            btn=findViewById(R.id.btn_advertisement);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(AdvertisementForm.this,AdPayment.class);
                    startActivity(i);
                    finish();
                }
            });
    }

    public void init(){
        rb_1 =  (RadioButton)findViewById(R.id.btn_radio1);
        rb_2 =  findViewById(R.id.btn_radio2);
        rb_3 =  findViewById(R.id.btn_radio3);
        rb_4 =  findViewById(R.id.btn_radio4);
        rb_1.setOnClickListener(this);
        rb_2.setOnClickListener(this);
        rb_3.setOnClickListener(this);
        rb_4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();
        switch (v.getId()) {
            case R.id.btn_radio1:
                if (checked) {
                    rb_2.setChecked(false);
                    rb_3.setChecked(false);
                    rb_4.setChecked(false);
                }
                break;
            case R.id.btn_radio2:
                if (checked) {
                    rb_1.setChecked(false);
                    rb_3.setChecked(false);
                    rb_4.setChecked(false);
                }
                break;
            case R.id.btn_radio3:
                if (checked) {
                    rb_1.setChecked(false);
                    rb_2.setChecked(false);
                    rb_4.setChecked(false);
                }
                break;
            case R.id.btn_radio4:
                if (checked) {
                    rb_1.setChecked(false);
                    rb_3.setChecked(false);
                    rb_2.setChecked(false);
                }
                break;
        }
    }
}
