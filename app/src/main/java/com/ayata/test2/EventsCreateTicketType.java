package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EventsCreateTicketType extends AppCompatActivity {

    private Button btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_create_ticket_type);

        //view ma text change in toolbar

        View headerLayout= findViewById(R.id.ectt_toolbar);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.tb_text);
        headerText.setText("Create Ticket Price");


        //nextbtn

        btn = findViewById(R.id.ectt_next_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("next","click");
                Intent i= new Intent(EventsCreateTicketType.this,EventCreateTicketBank.class);
                startActivity(i);
                finish();
            }
        });
    }
}
