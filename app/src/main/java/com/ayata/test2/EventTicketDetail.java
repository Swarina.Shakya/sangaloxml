package com.ayata.test2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EventTicketDetail extends AppCompatActivity {

    private Button btn;
    private ImageView im;
    private Drawable drawable;


    private Drawable d1;

    private Boolean bookmark= false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_ticket_detail);

        btn=findViewById(R.id.etd_btn_buy);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EventTicketDetail.this,EventTicketSelect.class);
                startActivity(intent);
                finish();
            }
        });

        im=findViewById(R.id.etd_bookmark);
        drawable=im.getDrawable();
        d1=getResources().getDrawable(R.drawable.ic_bookmark_black);

//        Drawable bg = view.getBackground();
//        if (drawable instanceof ColorDrawable){
//            color = ((ColorDrawable) drawable).getColor();
//        }

        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bookmark==false) {
                   bookmark=true;
                    // im.setImageDrawable(getResources().getDrawable(R.drawable.ic_bookmark_blue));
                    drawable.setTint(getResources().getColor(R.color.colorAccent));
                }
                else{
                    bookmark=false;
                    drawable.setTint(getResources().getColor(R.color.colorBlack));

                }
            }
        });

    }
}
