package com.ayata.test2;

import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

public class ModelEventCustomerList {

    int image;
    String name;
    String ticketno;

    public ModelEventCustomerList(int image, String name, String ticketno) {
        this.image = image;
        this.name = name;
        this.ticketno = ticketno;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTicketno() {
        return ticketno;
    }

    public void setTicketno(String ticketno) {
        this.ticketno = ticketno;
    }
}
