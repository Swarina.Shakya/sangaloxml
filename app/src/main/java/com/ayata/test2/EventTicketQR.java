package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EventTicketQR extends AppCompatActivity {

    private Button btn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_ticket_qr);

        btn=findViewById(R.id.etqr_btn_ok);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(EventTicketQR.this,EventCustomerDetailAll.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
