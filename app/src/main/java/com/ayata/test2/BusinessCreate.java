package com.ayata.test2;

import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.TextView;
        import androidx.appcompat.app.AppCompatActivity;

public class BusinessCreate extends AppCompatActivity {
    private TextView termsOs;
    private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.businesscrete);
//     termsOs =findViewById(R.id.terms_of_service);
//     String text ="By clicking Get Started, you agree to the our Terms of Service.";
//        SpannableStringBuilder ss= new SpannableStringBuilder(text);
//        ForegroundColorSpan fs=new ForegroundColorSpan(Color.BLUE);
//        ss.setSpan(fs,42,57, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//    termsOs.setText(ss);
        btn=findViewById(R.id.btn_businesscreate_next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BusinessCreate.this,BusinessCreateMap.class);
                startActivity(i);
                finish();
            }
        });

        View headerLayout= findViewById(R.id.bc_include);

        TextView headerText= (TextView)headerLayout.findViewById(R.id.tmn_textView11);
        headerText.setText("Create Business");
    }
}
