package com.ayata.test2;

import androidx.appcompat.app.AppCompatActivity;

public class ModelMyEvents {


    private int image;
    private String eventname;
    private String eventdate;


    public ModelMyEvents(int image, String eventname, String eventdate) {
        this.image = image;
        this.eventname = eventname;
        this.eventdate = eventdate;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public String getEventdate() {
        return eventdate;
    }

    public void setEventdate(String eventdate) {
        this.eventdate = eventdate;
    }



}
