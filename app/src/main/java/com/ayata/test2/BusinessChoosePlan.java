package com.ayata.test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BusinessChoosePlan extends AppCompatActivity {
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_businesschooseplan);
        View headerLayout = findViewById(R.id.bcp_include);

        TextView headerText=(TextView)headerLayout.findViewById(R.id.tmn_textView11);
        headerText.setText("Business Plan");
        btn=findViewById(R.id.btn_businesschooseplan_next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(BusinessChoosePlan.this,BusinessPayment.class);
                startActivity(i);
                finish();
            }
        });
    }


    @Override
    public void finish() {
        super.finish();
//        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}
