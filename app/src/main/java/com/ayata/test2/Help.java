package com.ayata.test2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.animation.MotionSpec;

import java.util.ArrayList;
import java.util.List;

public class Help extends AppCompatActivity implements AdapterHelp.OnHelpListener {

    private LinearLayoutManager linearLayoutManager;
    private List<ModelHelp> listitem;
    private AdapterHelp adapterHelp;
    private RecyclerView recyclerView;

   private ModelHelp data;

   private ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);

        View headerLayout= findViewById(R.id.help_toolbar);
        TextView headerText= (TextView)headerLayout.findViewById(R.id.appbar_text);
        headerText.setText("Help and Support");

        imageView = headerLayout.findViewById(R.id.back);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Help.this, Profile.class);
                startActivity(i);
                finish();
            }
        });

        recyclerView= (RecyclerView)findViewById(R.id.help_recycler_view);
        listitem= new ArrayList<>();
        adapterHelp=new AdapterHelp(this,listitem,this);
        linearLayoutManager= new LinearLayoutManager(this);
        recyclerView.setAdapter(adapterHelp);
        recyclerView.setLayoutManager(linearLayoutManager);
        DataPrepare();

    }

    private void DataPrepare(){

        data=new ModelHelp("How to change my password?", ModelHelp.HelpType.type_close);
        listitem.add(data);

//        data=new ModelHelp("How to change my password?", ModelHelp.HelpType.type_open);
//        listitem.add(data);
        data=new ModelHelp("How to change my profile picture?", ModelHelp.HelpType.type_close);
        listitem.add(data);
        data=new ModelHelp("How to delete my business page?", ModelHelp.HelpType.type_close);
        listitem.add(data);
        data=new ModelHelp("How to logout?", ModelHelp.HelpType.type_close);
        listitem.add(data);
        data=new ModelHelp("How to create my event?", ModelHelp.HelpType.type_close);
        listitem.add(data);
        data=new ModelHelp("How to edit my event?", ModelHelp.HelpType.type_close);
        listitem.add(data);


    }

    @Override
    public void onHelpClick(int position) {
        Log.d("help", "onHelpClick: "+position);
//        ModelHelp modelHelp= new ModelHelp("How to change my password?", ModelHelp.HelpType.type_open);
//        listitem.set(position,modelHelp);

        ModelHelp modelHelp = listitem.get(position);
        if(modelHelp.getHelpType()==ModelHelp.HelpType.type_open){
            modelHelp.setHelpType(ModelHelp.HelpType.type_close);
        }else if(modelHelp.getHelpType()==ModelHelp.HelpType.type_close){
            modelHelp.setHelpType(ModelHelp.HelpType.type_open);
        }else{
            Log.d("error", "onHelpClick:  type_open and type_close only ");
        }
        listitem.set(position,modelHelp);
        recyclerView.setAdapter(adapterHelp);
        recyclerView.setLayoutManager(linearLayoutManager);

    }
}
