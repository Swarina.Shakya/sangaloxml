package com.ayata.test2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterHelp  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ONE = 1;
    private static final int TYPE_TWO = 2;

    Context context;
    List<ModelHelp> listitem;
    private OnHelpListener onHelpListener;

    public AdapterHelp(Context context, List<ModelHelp> listitem, OnHelpListener onHelpListener) {
        this.context = context;
        this.listitem = listitem;
        this.onHelpListener = onHelpListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE_ONE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_help_close, parent, false);
            return new HelpCloseViewHolder(v, onHelpListener);
        } else if (viewType == TYPE_TWO) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_help_open, parent, false);
            return new HelpOpenViewHolder(v, onHelpListener);
        }else{
            throw new RuntimeException("The type has to be ONE or TWO");
        }


    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case TYPE_ONE:
                initLayoutHelpClose((HelpCloseViewHolder) holder,position);
                break;
            case TYPE_TWO:
                initLayoutHelpOpen((HelpOpenViewHolder) holder,position);
                break;
            default:
                break;
        }
    }

    private void initLayoutHelpClose(HelpCloseViewHolder holder, int position) {
        holder.question.setText(listitem.get(position).getQuestion());
    }

    private void initLayoutHelpOpen(HelpOpenViewHolder holder, int position) {
        holder.question.setText(listitem.get(position).getQuestion());
    }


    @Override
    public int getItemCount() {
        return listitem.size();
    }

    @Override
    public int getItemViewType(int position) {
        ModelHelp item = listitem.get(position);
        if(item.getHelpType()==ModelHelp.HelpType.type_close){
            return TYPE_ONE;
        }else if (item.getHelpType()==ModelHelp.HelpType.type_open){
            return TYPE_TWO;
        }else{
            return -1;
        }
    }

//    public
    static class HelpCloseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView question;
        AdapterHelp.OnHelpListener onHelpListener;

        public HelpCloseViewHolder(@NonNull View itemView, AdapterHelp.OnHelpListener onHelpListener) {
            super(itemView);
            question=(TextView)itemView.findViewById(R.id.help_question_close);
            this.onHelpListener=onHelpListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        onHelpListener.onHelpClick(getAdapterPosition());

        }
    }

    public interface OnHelpListener{
        void onHelpClick(int position);
    }

    //for open help
    static class HelpOpenViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView question;
        AdapterHelp.OnHelpListener onHelpListener;

        public HelpOpenViewHolder(@NonNull View itemView, AdapterHelp.OnHelpListener onHelpListener) {
            super(itemView);
            question=(TextView)itemView.findViewById(R.id.help_question_open);
            this.onHelpListener=onHelpListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onHelpListener.onHelpClick(getAdapterPosition());

        }
    }
}
