package com.ayata.test2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

public class AdapterEventCustomerList extends RecyclerView.Adapter<AdapterEventCustomerList.CustomerViewHolder> {

    Context context;
    List<ModelEventCustomerList> listitem;
    private OnCustomerListener onCustomerListener;

    public AdapterEventCustomerList(Context context, List<ModelEventCustomerList> listitem, OnCustomerListener onCustomerListener) {
        this.context = context;
        this.listitem = listitem;
        this.onCustomerListener=onCustomerListener;
    }

    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_event_customer_list,parent,false);
        return new CustomerViewHolder(v,onCustomerListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerViewHolder holder, int position) {

        holder.image.setImageResource(listitem.get(position).getImage());
        holder.name.setText(listitem.get(position).getName());
        holder.ticketno.setText(listitem.get(position).getTicketno());

    }

    @Override
    public int getItemCount() {
        return listitem.size();
    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        CircularImageView image;
        TextView name;
        TextView ticketno;
        OnCustomerListener onCustomerListener;

        public CustomerViewHolder(@NonNull View itemView, OnCustomerListener onCustomerListener) {
            super(itemView);
            image=(CircularImageView)itemView.findViewById(R.id.client_image);
            name=(TextView)itemView.findViewById(R.id.client_name);
            ticketno=(TextView)itemView.findViewById(R.id.client_ticketno);
            this.onCustomerListener=onCustomerListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            onCustomerListener.onCustomerClick(getAdapterPosition());

        }
    }

    public interface OnCustomerListener{
        void onCustomerClick(int position);
    }
}
