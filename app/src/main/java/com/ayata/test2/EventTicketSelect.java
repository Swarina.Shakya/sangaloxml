package com.ayata.test2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EventTicketSelect extends AppCompatActivity {

    private Button btn;
    private ImageView im;
    private Boolean bookmark=false;
    private Drawable drawable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_ticket_select);

        btn=findViewById(R.id.ets_btn_select);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EventTicketSelect.this,EventTicketpayment.class);
                startActivity(intent);
                finish();
            }
        });

        im=findViewById(R.id.ets_bookmark);
        drawable=im.getDrawable();

        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bookmark==false) {
                    bookmark=true;
                    // im.setImageDrawable(getResources().getDrawable(R.drawable.ic_bookmark_blue));
                    drawable.setTint(getResources().getColor(R.color.colorAccent));
                }
                else{
                    bookmark=false;
                    drawable.setTint(getResources().getColor(R.color.colorBlack));

                }
            }
        });


    }
}
