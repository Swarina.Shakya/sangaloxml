package com.ayata.test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BusinessPayment extends AppCompatActivity {
private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_payment);
        View headerLayout = findViewById(R.id.bp_include);

        TextView headerText=(TextView)headerLayout.findViewById(R.id.tmn_textView11);
        headerText.setText("Plan Payment Method");
        btn=findViewById(R.id.btn_businesspayment_next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(BusinessPayment.this,BusinessCardDetails.class);
                startActivity(i);
                finish();
            }
        });
    }
}
