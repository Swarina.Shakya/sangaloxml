package com.ayata.test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UserLogin extends AppCompatActivity {
private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlogin);
                View headerLayout= findViewById(R.id.user_login_include);
        TextView headerText= (TextView)headerLayout.findViewById(R.id.login);
        headerText.setText("User Login");

        View login= findViewById(R.id.user_login_include);
        btn=login.findViewById(R.id.button_login);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(UserLogin.this,Profile.class);
                startActivity(i);
                finish();
            }
        });

        TextView textView= login.findViewById(R.id.signup_text);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(UserLogin.this,Verification.class);
                startActivity(i);
                finish();

            }
        });
    }
}
